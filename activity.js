/*

	Activity:

	Update and Debug the following codes to ES6.
		Use template literals,
		Use array/object destructuring,
		Use arrow functions
	
	Create a class constructor able to receive 3 arguments
		-It should be able to receive two strings and a number
		-Using the this keyword assign properties:
			name, 
			breed, 
			dogAge = <7x human years> 
			-assign the parameters as values to each property.

	Create 2 new objects using our constructor.

	This constructor should be able to create Dog objects.

	Log the 2 new Dog objects in the console.


*/
let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101","Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402",]
}

function introduce(student){

	console.log(`Hi I'm ${student.name}. I am ${student.age} years old.`);

	console.log(`I study the following courses ${student.classes}`);
}

introduce(student2)

const getCube = (num) => Math.pow(num,3)

let cube = getCube(3);

console.log(cube);

let numArr = [15,16,32,21,21,2];

numArr.forEach((num) => {
	console.log(`The numbers are: ${num}`);
});


let numsSquared = numArr.map((num) => Math.pow(num,2))


console.log(numsSquared);

class dog{
	constructor(name,breed,dogAge){
		this.name = name;
		this.breed = breed;
		this.dogAge = dogAge * 7;
	}
};

let dog1 = new dog("Loki","Pitbull Showtype",6)
let dog2 = new dog("Luki","Shih Tzu",2)

console.log(dog1)
console.log(dog2)